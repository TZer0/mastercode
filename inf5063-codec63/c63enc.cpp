#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "c63.h"
#include "tables.h"

#define LIMIT(val, bound) (val < -bound ? -bound : (val > bound ? bound : val))

static char *output_file, *mvf_file, *input_file = NULL;
FILE *outfile;

static int limit_numframes = 0;

static uint32_t width;
static uint32_t height;

/* getopt */
extern int optind;
extern char *optarg;

/* Read planar YUV frames with 4:2:0 chroma sub-sampling */
static yuv_t* read_yuv(FILE *file, FILE *mvf_source, struct c63_common *cm)
{
	size_t len = 0;
	size_t mv_len = 0;
	yuv_t *image = new yuv_t;

	/* Read Y. The size of Y is the same as the size of the image. The indices
	   represents the color component (0 is Y, 1 is U, and 2 is V) */
	image->Y = new uint8_t[cm->padw[0]*cm->padh[0]];
	len += fread(image->Y, 1, width*height, file);

	/* Read U. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y
	   because (height/2)*(width/2) = (height*width)/4. */
	image->U =  new uint8_t[cm->padw[1]*cm->padh[1]];
	len += fread(image->U, 1, (width*height)/4, file);

	/* Read V. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y. */
	image->V = new uint8_t[cm->padw[2]*cm->padh[2]];
	len += fread(image->V, 1, (width*height)/4, file);

	size_t ysize = (cm->padw[0] * cm->padh[0] * 2 * sizeof(int16_t))/64;
	size_t uvsize = (cm->padw[1] * cm->padh[1] * 2 * sizeof(int16_t))/64;
	int tmp, error = 0;
	if (mvf_source != NULL) {
		image->MVY = new int16_t[ysize];
		image->MVUV = new int16_t[uvsize];
		tmp = fread(image->MVY, 1, ysize, mvf_source);
		if (tmp < 0) {
			error = 1;
		}
		mv_len += tmp;
		tmp = fread(image->MVUV, 1, uvsize, mvf_source);
		if (tmp < 0) {
			error = 1;
		}
		mv_len += tmp;
	} else {
		image->MVY = image->MVUV = NULL;
	}

	if (ferror(file) || (mvf_source != NULL && ferror(mvf_source)))
	{
		perror("ferror");
		exit(EXIT_FAILURE);
	}

	if ((mvf_source != NULL && error) || len != width*height*1.5 || (mvf_source != NULL && mv_len != (ysize+uvsize)))
	{
		if (!feof(file) ) {
			fprintf(stderr, "Reached end of file, but incorrect bytes read.\n");
			fprintf(stderr, "Wrong input? (height: %d width: %d)\n", height, width);
		}


		free(image->Y);
		free(image->U);
		free(image->V);
		if (mvf_source != NULL) {
			free(image->MVY);
			free(image->MVUV);
		}
		free(image);

		return NULL;
	}

	return image;
}

static void c63_motion_sub_read(struct c63_common *cm, int cc) {
	int16_t *mv = cm->curframe->orig->MVUV;
	if (cc == 0) {
		mv = cm->curframe->orig->MVY;
	}
	int i, j;
	for (i = 0; i < cm->padh[cc]/8; i++) {
		for (j = 0; j < cm->padw[cc]/8; j++) {
			struct macroblock *mb = &cm->curframe->mbs[cc][i * cm->padw[cc]/8 + j];
			int mv_x = mv[(i*cm->padw[cc]/8 + j)*2];
			int mv_y = mv[(i*cm->padw[cc]/8 + j)*2 + 1];

			mv_x = LIMIT(mv_x, 1024);
			mv_y = LIMIT(mv_y, 1024);
			
			while (mv_x + j * 8 <= 0) {
				mv_x++;
			}
			while (mv_y + i * 8 <= 0) {
				mv_y++;
			}
			while (mv_x + j * 8 >= cm->padw[cc]-8) {
				mv_x--;
			}
			while (mv_y + i * 8 >= cm->padh[cc]-8) {
				mv_y--;
			}
			mb->mv_x = mv_x;
			mb->mv_y = mv_y;

			mb->use_mv = 1;
		}
	}
}

static void c63_motion_read(struct c63_common *cm) {
	c63_motion_sub_read(cm, 0);
	c63_motion_sub_read(cm, 1);
	c63_motion_sub_read(cm, 2);
}

void c63_encode_image(struct c63_common *cm, yuv_t *image)
{
	/* Advance to next frame */
	destroy_frame(cm->refframe);
	cm->refframe = cm->curframe;
	cm->curframe = create_frame(cm, image);

	/* Check if keyframe */
	if (cm->framenum == 0 || cm->frames_since_keyframe == cm->keyframe_interval)
	{
		cm->curframe->keyframe = 1;
		cm->frames_since_keyframe = 0;

		fprintf(stderr, " (keyframe) ");
	}
	else { cm->curframe->keyframe = 0; }

	if (!cm->curframe->keyframe)
	{
		/* Motion Estimation */
		if (cm->curframe->orig->MVY == NULL || cm->curframe->orig->MVUV == NULL) {
			c63_motion_estimate(cm);
		} else {
			c63_motion_read(cm);
		}

		/* Motion Compensation */
		c63_motion_compensate(cm);
	}

	/* DCT and Quantization */
#if 0
#if 0
	dct_quantize(image->Y, cm->curframe->predicted->Y, cm->padw[0], cm->padh[0],
			cm->curframe->residuals->Ydct, cm->quanttbl[0]);
	dequantize_idct(cm->curframe->residuals->Ydct, cm->curframe->predicted->Y,
			cm->ypw, cm->yph, cm->curframe->recons->Y, cm->quanttbl[0]);
#else
	dct_quantize(image->U, cm->curframe->predicted->U, cm->padw[1], cm->padh[1],
			cm->curframe->residuals->Udct, cm->quanttbl[1]);
	dct_quantize(image->V, cm->curframe->predicted->V, cm->padw[2], cm->padh[2],
			cm->curframe->residuals->Vdct, cm->quanttbl[2]);
	dequantize_idct(cm->curframe->residuals->Udct, cm->curframe->predicted->U,
			cm->upw, cm->uph, cm->curframe->recons->U, cm->quanttbl[1]);
	dequantize_idct(cm->curframe->residuals->Vdct, cm->curframe->predicted->V,
			cm->vpw, cm->vph, cm->curframe->recons->V, cm->quanttbl[2]);
#endif
#else
	dct_quantize(image->Y, cm->curframe->predicted->Y, cm->padw[0], cm->padh[0],
			cm->curframe->residuals->Ydct, cm->quanttbl[0]);
	dequantize_idct(cm->curframe->residuals->Ydct, cm->curframe->predicted->Y,
			cm->ypw, cm->yph, cm->curframe->recons->Y, cm->quanttbl[0]);
	dct_quantize(image->U, cm->curframe->predicted->U, cm->padw[1], cm->padh[1],
			cm->curframe->residuals->Udct, cm->quanttbl[1]);
	dct_quantize(image->V, cm->curframe->predicted->V, cm->padw[2], cm->padh[2],
			cm->curframe->residuals->Vdct, cm->quanttbl[2]);
	dequantize_idct(cm->curframe->residuals->Udct, cm->curframe->predicted->U,
			cm->upw, cm->uph, cm->curframe->recons->U, cm->quanttbl[1]);
	dequantize_idct(cm->curframe->residuals->Vdct, cm->curframe->predicted->V,
			cm->vpw, cm->vph, cm->curframe->recons->V, cm->quanttbl[2]);
#endif

	/* Reconstruct frame for inter-prediction */

	/* Function dump_image(), found in common.c, can be used here to check if the
	   prediction is correct */

	write_frame(cm);

	++cm->framenum;
	++cm->frames_since_keyframe;
}

struct c63_common* init_c63_enc(int width, int height, int sr)
{
	int i;

	/* calloc() sets allocated memory to zero */
	struct c63_common *cm = new struct c63_common;
	memset(cm, 0, sizeof(struct c63_common));

	cm->width = width;
	cm->height = height;

	cm->padw[0] = cm->ypw = (uint32_t)(ceil(width/16.0f)*16);
	cm->padh[0] = cm->yph = (uint32_t)(ceil(height/16.0f)*16);
	cm->padw[1] = cm->upw = (uint32_t)(ceil(width*UX/(YX*8.0f))*8);
	cm->padh[1] = cm->uph = (uint32_t)(ceil(height*UY/(YY*8.0f))*8);
	cm->padw[2] = cm->vpw = (uint32_t)(ceil(width*VX/(YX*8.0f))*8);
	cm->padh[2] = cm->vph = (uint32_t)(ceil(height*VY/(YY*8.0f))*8);

	cm->mb_cols = cm->ypw / 8;
	cm->mb_rows = cm->yph / 8;

	/* Quality parameters */
	cm->qp = 25;                  // Constant quantization factor. Range: [1..50]
	cm->me_search_range = sr;     // Pixels in every direction
	cm->keyframe_interval = 100;  // Distance between keyframes

	/* Initialize quantization tables */
	for (i = 0; i < 64; ++i)
	{
		cm->quanttbl[0][i] = yquanttbl_def[i] / (cm->qp / 10.0);
		cm->quanttbl[1][i] = uvquanttbl_def[i] / (cm->qp / 10.0);
		cm->quanttbl[2][i] = uvquanttbl_def[i] / (cm->qp / 10.0);
	}

	return cm;
}

static void print_help()
{
	printf("Usage: ./c63enc [options] input_file\n");
	printf("Commandline options:\n");
	printf("  -h                             Height of images to compress\n");
	printf("  -w                             Width of images to compress\n");
	printf("  -o                             Output file (.c63)\n");
	printf("  [-f]                           Limit number of frames to encode\n");
	printf("\n");

	exit(EXIT_FAILURE);
}

#ifdef C63_MAIN
int main(int argc, char **argv)
{
	int c;
	yuv_t *image;

	if (argc == 1) { print_help(); }
	int sr = 16;

	while ((c = getopt(argc, argv, "h:w:o:f:i:m:s:")) != -1)
	{
		switch (c)
		{
			case 'h':
				height = atoi(optarg);
				break;
			case 'w':
				width = atoi(optarg);
				break;
			case 'o':
				output_file = optarg;
				break;
			case 'f':
				limit_numframes = atoi(optarg);
				break;
			case 'm':
				mvf_file = optarg;
				break;
			case 's':
				sr = atoi(optarg);
				break;
			default:
				print_help();
				break;
		}
	}

	if (optind >= argc)
	{
		fprintf(stderr, "Error getting program options, try --help.\n");
		exit(EXIT_FAILURE);
	}

	outfile = fopen(output_file, "wb");

	if (outfile == NULL)
	{
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	struct c63_common *cm = init_c63_enc(width, height, sr);
	cm->e_ctx.fp = outfile;

	input_file = argv[optind];

	if (limit_numframes) { printf("Limited to %d frames.\n", limit_numframes); }

	FILE *mvf_source = NULL;
	if (mvf_file != NULL) {
		mvf_source = fopen(mvf_file, "rb");
		if (mvf_source <= 0) {
			printf("Error opening file %s\n", mvf_file);
			return -1;
		}
	}

	FILE *infile = fopen(input_file, "rb");
	

	if (infile == NULL)
	{
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	/* Encode input frames */
	int numframes = 0;

	while (1)
	{
		image = read_yuv(infile, mvf_source, cm);

		if (!image) { break; }

		printf("Encoding frame %d, ", numframes);
		c63_encode_image(cm, image);

		//free(image->Y);
		//free(image->U);
		//free(image->V);
		//free(image->MVY);
		//free(image->MVUV);
		//free(image);

		printf("Done!\n");

		++numframes;

		if (limit_numframes && numframes >= limit_numframes) { break; }
	}

	fclose(outfile);
	fclose(infile);
	if (mvf_source != NULL) {
		fclose(mvf_source);
	}

	//int i, j;
	//for (i = 0; i < 2; ++i)
	//{
	//  printf("int freq[] = {");
	//  for (j = 0; j < ARRAY_SIZE(frequencies[i]); ++j)
	//  {
	//    printf("%d, ", frequencies[i][j]);
	//  }
	//  printf("};\n");
	//}

	return EXIT_SUCCESS;
}
#endif
