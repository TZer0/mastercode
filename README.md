For every next-level subfolder with a LICENCE or COPYING file, that file applies recursively as the license.

For any next-level subfolder without a LICENSE or COPYING file, the LICENSE file in this folder applies recursively as the license.

alltest.sh was used when running the tests in the x264-encoder.
make encode[vale[mvf]|mvf] was used when running the tests for the inf5063-c63 encoder.

The source code has only been tested on Linux with the exception of SimpleGL which was only tested on Windows.

Build instructions, notes and other instructions are provided in each subfolder in the README.md-files and INSTRUCTION-files. The README.md in ValeDecem is made by the original authors of the project. Remember that certain IDEs such as QTCreator can open CMakeLists.txt as project files in addition to the .pro-format.
