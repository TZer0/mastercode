#ifndef UTIL_H
#define UTIL_H
#include <string>
#include <iostream>

#include <GL/glew.h>

void checkError(std::string msg);
void convertToYCbCrArrays(GLubyte *input, uint8_t *Y, uint8_t *Cb, uint8_t *Cr, int width, int height);
void convertToMVArrays(GLfloat *input, int16_t *mv_y, int16_t *mv_cbcr, int stride, int height, int width);

#endif // UTIL_H
