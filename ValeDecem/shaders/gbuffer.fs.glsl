#version 330 core

in vec3 v_Normal;
in vec2 v_UV;
in vec4 v_new_pos;
in vec4 v_old_pos;


uniform sampler2D Diffuse;
uniform sampler2D Specular;

layout(location = 0) out vec4 Color;
layout(location = 1) out vec4 Normal;
layout(location = 2) out vec2 MovVal;

void main(void)
{
	float spec = texture(Specular, v_UV).r;
	
	Color = texture(Diffuse, v_UV);
	Normal = vec4(v_Normal, spec);

	vec3 oldScrPos = (v_old_pos/v_old_pos.w).xyz;
	vec3 scrPos = (v_new_pos/v_new_pos.w).xyz;
	MovVal = (scrPos - oldScrPos).xy;
}
