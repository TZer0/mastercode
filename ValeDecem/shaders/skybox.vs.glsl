#version 330 core

layout(location = 0)in vec3 Position;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;
uniform mat4 OView;
uniform mat4 OModel;

out vec3 v_UV;
out vec4 v_new_pos;
out vec4 v_old_pos;

void main(){
        gl_Position = (Projection * View * Model * vec4(Position, 1.0)).xyww;
        v_UV = Position;

        v_new_pos = gl_Position;
        v_old_pos = Projection * OView * OModel * vec4(Position, 1.0);
}
