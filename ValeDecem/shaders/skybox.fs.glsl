#version 330 core

in vec3 v_UV;
in vec4 v_new_pos;
in vec4 v_old_pos;

uniform samplerCube SkyboxTexture;

out vec4 Color;

void main()
{
    Color = texture(SkyboxTexture, v_UV);
}
