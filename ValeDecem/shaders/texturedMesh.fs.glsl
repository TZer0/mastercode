#version 330 core
#extension GL_ARB_explicit_uniform_location : enable

in vec3 v_Normal;
in vec2 v_UV;
in vec4 v_new_pos;
in vec4 v_old_pos;

layout(location = 11) uniform sampler2D Material;
layout(location = 12) uniform float Specular;

layout(location = 0) out vec4 Color;
layout(location = 1) out vec4 Normal;
layout(location = 2) out vec2 MovVal;

void main(void)
{
        Color = texture(Material, v_UV);
        Normal = vec4(v_Normal, Specular);

        vec3 oldScrPos = (v_old_pos/v_old_pos.w).xyz;
        vec3 scrPos = (v_new_pos/v_new_pos.w).xyz;
        MovVal = (scrPos - oldScrPos).xy;
}
