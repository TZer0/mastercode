#version 330 core
#extension GL_ARB_explicit_uniform_location : enable

layout(location = 0)in vec3 Position;
layout(location = 1)in vec3 Normal;
layout(location = 2)in vec2 UV;

layout(location = 0) uniform mat4 Projection;
layout(location = 1) uniform mat4 View;
layout(location = 2) uniform mat4 Model;
layout(location = 3) uniform mat4 OView;
layout(location = 4) uniform mat4 OModel;


out vec3 v_Normal;
out vec2 v_UV;
out vec4 v_new_pos;
out vec4 v_old_pos;

void main(void)
{	
        gl_Position = Projection * View * Model * vec4(Position, 1.0);
        v_new_pos = gl_Position;
        v_old_pos = Projection * OView * OModel * vec4(Position, 1.0);

	v_Normal = (Model * vec4(Normal, 0.0)).xyz;
	v_UV = UV;
}
