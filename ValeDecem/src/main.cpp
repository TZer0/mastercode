#include <Project.h>

/*!
 * ValeDecem - Main function
 */
int main()
{
	//int width = 1280, height = 720;
	int width = 1024, height = 768;

	Project ourProject(width, height, "Project d'OpenGL");
	ourProject.init();
	ourProject.run();

	return EXIT_SUCCESS;
}
