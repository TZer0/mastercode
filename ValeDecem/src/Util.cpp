#include <Util.h>
#include <math.h>

#define WINDOWWIDTH width
#define WINDOWHEIGHT height

#define YR 0.257
#define YG 0.504
#define YB 0.098

#define CRR 0.439
#define CRG -0.368
#define CRB -0.071

#define CBR -0.148
#define CBG -0.291
#define CBB 0.439

#define YS 16
#define CS 128

#define YVAL(buf, index) (buf[index] * YR + buf[index+1] * YG + buf[index+2] * YB + YS)

#define CRVAL(buf, index) (0.25 * (\
	CRSUBVAL(buf, index) + \
	CRSUBVAL(buf, index+4) +\
	CRSUBVAL(buf, index+width*4) +\
	CRSUBVAL(buf, index+width*4+4)\
	))

#define CRSUBVAL(buf, index) (buf[index] * CRR + buf[index+1] * CRG + buf[index+2] * CRB + CS)


#define CBVAL(buf, index) (0.25 * (\
	CBSUBVAL(buf, index) + \
	CBSUBVAL(buf, index+4) +\
	CBSUBVAL(buf, index+width*4) +\
	CBSUBVAL(buf, index+width*4+4)\
	))

#define CBSUBVAL(buf, index) (buf[index] * CBR + buf[index+1] * CBG + buf[index+2] * CBB + CS)



void checkError(std::string msg) {
	GLenum error = glGetError();
	if (error != GL_NO_ERROR) {
		std::cout << msg << ": " << gluErrorString(error) << std::endl;
	}

}


void convertToYCbCrArrays(GLubyte *input, uint8_t *Y, uint8_t *Cb, uint8_t *Cr, int width, int height) {
	uint current = 0;
	int i, j;
	for (i = height-1; i >= 0; i--) {
		for (j = 0; j < width; j++) {
			Y[current++] = YVAL(input, i*width*4 + j*4);
		}
	}
	current = 0;
	for (i = height-2; i >= 0; i-=2) {
		for (j = 0; j < width; j+=2) {
			Cb[current++] = CBVAL(input, i*width*4 + j*4);
		}
	}
	current = 0;
	for (i = height-2; i >= 0; i-=2) {
		for (j = 0; j < width; j+=2) {
			Cr[current++] = CRVAL(input, i*width*4 + j*4);
		}
	}
}

void convertToMVArrays(GLfloat *input, int16_t *mv_y, int16_t *mv_cbcr, int stride, int width, int height) {
	int i, j, index, current;
	current = 0;
	for (i = height-stride/2 + 1; i >= 0; i -= stride) {
		for (j = stride/2 - 1; j < width; j += stride) {
			index = (i*width + j) * 2;
			mv_y[current++] = round(-input[index]*width/2.);
			mv_y[current++] = round(input[index+1]*height/2.);
		}
	}
	current = 0;
	for (i = height-stride + 1; i >= 0; i -= stride * 2) {
		for (j = stride - 1; j < width; j += stride * 2) {
			index = (i*width + j) * 2;
			mv_cbcr[current++] = round(-input[index] * width/4.);
			mv_cbcr[current++] = round(input[index+1] * height/4.);
		}
	}
}
