#define WINDOWWIDTH 1024
#define WINDOWHEIGHT 768

#define YR 0.257
#define YG 0.504
#define YB 0.098

#define CRR 0.439
#define CRG -0.368
#define CRB -0.071

#define CBR -0.148
#define CBG -0.291
#define CBB 0.439

#define YS 16
#define CS 128

#define YVAL(buf, index) (buf[index] * YR + buf[index+1] * YG + buf[index+2] * YB + YS)

#define CRVAL(buf, index) (0.25 * (\
	CRSUBVAL(buf, index) + \
	CRSUBVAL(buf, index+3) +\
	CRSUBVAL(buf, index+WINDOWWIDTH*3) +\
	CRSUBVAL(buf, index+WINDOWWIDTH*3+3)\
	))

#define CRSUBVAL(buf, index) (buf[index] * CRR + buf[index+1] * CRG + buf[index+2] * CRB + CS)


#define CBVAL(buf, index) (0.25 * (\
	CBSUBVAL(buf, index) + \
	CBSUBVAL(buf, index+3) +\
	CBSUBVAL(buf, index+WINDOWWIDTH*3) +\
	CBSUBVAL(buf, index+WINDOWWIDTH*3+3)\
	))

#define CBSUBVAL(buf, index) (buf[index] * CBR + buf[index+1] * CBG + buf[index+2] * CBB + CS)
