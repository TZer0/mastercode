// Include standard headers
#include<stdio.h>
#include<stdlib.h>
#include<vector>
#include<iostream>
#include<fstream>
#include<stdint.h>
#include<string.h>
#include<getopt.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GL/glfw.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <common/shader.hpp>
#include <common/texture.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

#include "common.h"

#include "../../inf5063-codec63/c63.h"

static char short_options[] = "l:xm";

static struct option long_options[] = 
{
	{ "live",	required_argument, NULL, 'l' },
	{ "x264",	no_argument, NULL, 'x' },
	{ "mv",		no_argument, NULL, 'm' }
};

using namespace glm;

void checkError(std::string msg) {
	GLenum error = glGetError();
	if (error != GL_NO_ERROR) {
		std::cout << msg << ": " << gluErrorString(error) << std::endl;
	}

}

// Integrate with encoder
bool live_encoder;
struct c63_common *cm;

#define NUMOBJS 2
GLuint vertexbuffer[NUMOBJS];
GLuint uvbuffer[NUMOBJS];
GLuint normalbuffer[NUMOBJS];
GLuint elementbuffer[NUMOBJS];
std::vector<GLfloat *> mv_vec;
std::vector<GLubyte *> color_vec;

size_t indice_size[NUMOBJS];

void writeMovVec(std::ofstream *str, int stride, int v_el) {
	int16_t out[2];
	int i, j, index;
	for (i = WINDOWHEIGHT-stride/2 + 1; i >= 0; i -= stride) {
		for (j = stride/2 - 1; j < WINDOWWIDTH; j += stride) {
			index = (i*WINDOWWIDTH + j) * 2;
			out[0] = round(-mv_vec[v_el][index]*WINDOWWIDTH/2);
			out[1] = round(mv_vec[v_el][index+1]*WINDOWHEIGHT/2);
			str->write((char *) out, sizeof(int16_t)*2);
		}
	}

	for (i = WINDOWHEIGHT-stride + 1; i >= 0; i -= stride * 2) {
		for (j = stride - 1; j < WINDOWWIDTH; j += stride * 2) {
			index = (i*WINDOWWIDTH + j) * 2;
			out[0] = round(-mv_vec[v_el][index]*WINDOWWIDTH/4);
			out[1] = round(mv_vec[v_el][index+1] * WINDOWHEIGHT/4);
			str->write((char *) out, sizeof(int16_t)*2);
		}
	}
	str->flush();
}

void loadObjFile(std::string path, size_t arrayPos) {
	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJ(path.c_str(), vertices, uvs, normals);

	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

	// Load it into a VBO

	checkError("afterinit");
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[arrayPos]);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer[arrayPos]);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer[arrayPos]);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer[arrayPos]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
	indice_size[arrayPos] = indices.size();
}

void drawObj(size_t arrayPos) {
	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[arrayPos]);
	glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer[arrayPos]);
	glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer[arrayPos]);
	glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer[arrayPos]);


	// Draw the triangles !
	glDrawElements(
			GL_TRIANGLES,      // mode
			indice_size[arrayPos],    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
		      );

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void convertToYUVArrays(GLubyte *input, uint8_t *Y, uint8_t *U, uint8_t *V) {
	int current = 0;
	int i, j;
	for (i = WINDOWHEIGHT-1; i >= 0; i--) {
		for (j = 0; j < WINDOWWIDTH; j++) {
			Y[current++] = YVAL(input, i*WINDOWWIDTH*3 + j*3);
		}
	}
	current = 0;
	for (i = WINDOWHEIGHT-1; i >= 0; i-=2) {
		for (j = 0; j < WINDOWWIDTH; j+=2) {
			U[current++] = CBVAL(input, i*WINDOWWIDTH*3 + j*3);
		}
	}
	current = 0;
	for (i = WINDOWHEIGHT-1; i >= 0; i-=2) {
		for (j = 0; j < WINDOWWIDTH; j+=2) {
			V[current++] = CRVAL(input, i*WINDOWWIDTH*3 + j*3);
		}
	}
}

void convertToMVArrays(GLfloat *input, int16_t *mv_y, int16_t *mv_uv, int stride) {
	int i, j, index, current;
	current = 0;
	for (i = WINDOWHEIGHT - stride/2 + 1; i >= 0; i -= stride) {
		for (j = stride/2 - 1; j < WINDOWWIDTH; j += stride) {
			index = (i*WINDOWWIDTH + j) * 2;
			mv_y[current++] = round(-input[index]*WINDOWWIDTH/2);
			mv_y[current++] = round(input[index+1]*WINDOWHEIGHT/2);
		}
	}
	current = 0;
	for (i = WINDOWHEIGHT - stride + 1; i >= 0; i -= stride * 2) {
		for (j = stride/2 - 1; j < WINDOWWIDTH; j += stride * 2) {
			index = (i*WINDOWWIDTH + j) * 2;
			mv_uv[current++] = round(-input[index]*WINDOWWIDTH/4);
			mv_uv[current++] = round(input[index+1] * WINDOWHEIGHT/4);
		}
	}
}


int main( int argc, char **argv )
{
	bool buffer_init = false, use_mv = false;
	live_encoder = false;
	FILE *outfile;
	
	for ( optind = 0;; )
	{
		int c = getopt_long(argc, argv, short_options, long_options, NULL);
		switch(c) {
			case 'l':
				printf("Live\n");
				live_encoder = true;
				outfile = fopen(optarg, "wb");
				if (outfile == NULL) {
					printf("Can't write to destination\n");
					return -1;
				}
				cm = init_c63_enc(WINDOWWIDTH, WINDOWHEIGHT, 16);
				cm->e_ctx.fp = outfile;
				break;
			case 'm':
				printf("Motion vectors enabled\n");
				use_mv = true;
				break;
			default:
				break;
		}

		if (c == -1) {
			break;
		}
	}

	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}


	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	std::ofstream colorVals, vertexVals, movVecVals8, movVecVals16;
	colorVals.open("out.yuv", std::fstream::out);
	vertexVals.open("vertexVals.txt", std::fstream::out);
	movVecVals8.open("movVals8.mvf", std::fstream::out);
	movVecVals16.open("movVals16.mvf", std::fstream::out);

	checkError("preopen");
	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( WINDOWWIDTH, WINDOWHEIGHT, 0,0,0,0, 32,0, GLFW_WINDOW ) )
	{
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	checkError("postopen");

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}
	checkError("glew");

	glfwSetWindowTitle( "c63 demo" );

	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );
	glfwSetMousePos(WINDOWWIDTH/2, WINDOWHEIGHT/2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.7f, 0.0f);

	checkError("preenable");
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	checkError("preshade");

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "StandardShadingRTT.vertexshader", "StandardShadingRTT.fragmentshader" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint OldMatrixID = glGetUniformLocation(programID, "OMVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
	checkError("postshade");

	// Load the texture
	GLuint Texture = loadDDS("../commonres/uvmap.DDS");

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");

	// place read here
	glGenBuffers(2, elementbuffer);
	glGenBuffers(2, normalbuffer);
	glGenBuffers(2, uvbuffer);
	glGenBuffers(2, vertexbuffer);
	loadObjFile("../commonres/suzanne.obj", 0);
	loadObjFile("../commonres/sphere.obj", 1);

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");
	checkError("afterbuffer");


	// ---------------------------------------------
	// Render to Texture - specific code begins here
	// ---------------------------------------------

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	GLuint FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	checkError("renderText0");

	// The texture we're going to render to
	GLuint renderedTexture;
	glGenTextures(1, &renderedTexture);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, renderedTexture);

	// Give an empty image to OpenGL ( the last "0" means "empty" )
	checkError("renderText1");
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
	checkError("renderText2");

	// Poor filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	checkError("renderText");
	// The depth buffer
	/*GLuint depthrenderbuffer;
	  glGenRenderbuffers(1, &depthrenderbuffer);
	  glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, WINDOWWIDTH, WINDOWHEIGHT);
	  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);
	  checkError("depthRend");*/

	// Alternative : Depth texture. Slower, but you can sample it later in your shader

	GLuint depthTexture[3];
	glGenTextures(3, depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture[0]);
	//glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
	GLint *buffer = new GLint[WINDOWWIDTH*WINDOWHEIGHT];
	for (size_t i = 0; i < WINDOWWIDTH*WINDOWHEIGHT; i++) {
		buffer[i] = (GLint) i;
	}
	checkError("preTex");
	//glTexImage2D(GL_TEXTURE_2D, 0,GL_R32I, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RED, GL_INT, 0);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_R32I, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RED_INTEGER, GL_INT, buffer);
	checkError("depthtex");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, depthTexture[1]);
	//glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
	checkError("preTex2");
	//glTexImage2D(GL_TEXTURE_2D, 0,GL_R32I, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RED, GL_INT, 0);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_R32I, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RED_INTEGER, GL_INT, buffer);
	checkError("depthtex2");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, depthTexture[2]);

	//glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
	checkError("preTex3");	
	//glTexImage2D(GL_TEXTURE_2D, 0,GL_R32I, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RED, GL_INT, 0);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RG32F, WINDOWWIDTH, WINDOWHEIGHT, 0,GL_RG, GL_FLOAT, NULL);
	checkError("depthtex3");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


	// Set "renderedTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

	//// Depth texture alternative : 
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, depthTexture[0], 0);

	//// Depth texture alternative : 
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, depthTexture[1], 0);

	//// Depth texture alternative : 
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, depthTexture[2], 0);

	// Set the list of draw buffers.
	GLenum DrawBuffers[4] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
	glDrawBuffers(4, DrawBuffers);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return false;


	// The fullscreen quad's FBO
	static const GLfloat g_quad_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f,  1.0f, 0.0f,
	};

	GLuint quad_vertexbuffer;
	glGenBuffers(1, &quad_vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	// Create and compile our GLSL program from the shaders
	GLuint quad_programID = LoadShaders( "Passthrough.vertexshader", "WobblyTexture.fragmentshader" );
	GLuint texID = glGetUniformLocation(quad_programID, "renderedTexture");
	GLuint timeID = glGetUniformLocation(quad_programID, "time");


	//GLint buffer[WINDOWWIDTH*WINDOWHEIGHT];
	for (int i = 0; i < WINDOWWIDTH*WINDOWHEIGHT; i++) {
		buffer[i] = 0;
	}
	/*GLubyte buffer[WINDOWWIDTH*WINDOWHEIGHT*3];
	  for (int i = 0; i < WINDOWWIDTH*WINDOWHEIGHT*3; i++) {
	  buffer[i] = 0;
	  }*/

	buffer[0] = 1;
	bool first = true;
	GLubyte *colBuf;
	GLfloat *floatBuf;
	do{

		checkError("RenderStart");
		// Render to our framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
		glViewport(0,0,WINDOWWIDTH,WINDOWHEIGHT); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		if (first) {
			glUniformMatrix4fv(OldMatrixID, 1, GL_FALSE, &MVP[0][0]);
			first = false;
		}
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
		checkError("Render0");

		glm::vec3 lightPos = glm::vec3(4,4,4);
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(TextureID, 0);

		drawObj(1);
		drawObj(0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthTexture[0]);
		//glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, buffer);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_INT, buffer);
		checkError("Render1");
		//glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		//vertexVals.write((char *)buffer, WINDOWHEIGHT*WINDOWWIDTH*sizeof(GLint));
		if (!live_encoder || (live_encoder && !buffer_init)) {
			colBuf = new GLubyte[WINDOWWIDTH*WINDOWHEIGHT*3];
			floatBuf = new GLfloat[WINDOWWIDTH*WINDOWHEIGHT*2];
			buffer_init = true;
		}
		for (size_t i = 0; i < WINDOWWIDTH*WINDOWHEIGHT*2; i++) {
			floatBuf[i] = 999999.0f;
		}
		for (size_t i = 0; i < WINDOWWIDTH*WINDOWHEIGHT*3; i++) {
			colBuf[i] = (GLubyte) 0;
		}
		glBindTexture(GL_TEXTURE_2D, renderedTexture);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, colBuf);
		checkError("Render2");
		//colorVals.write((char *)colBuf, WINDOWHEIGHT*WINDOWWIDTH*3);
		glBindTexture(GL_TEXTURE_2D, depthTexture[2]);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RG, GL_FLOAT, floatBuf);
		if (live_encoder) {
			int stride = 8;
			yuv_t *image = new yuv_t;
			image->Y = new uint8_t[cm->padw[0] * cm->padh[0]];
			image->U = new uint8_t[cm->padw[1] * cm->padh[1]];
			image->V = new uint8_t[cm->padw[2] * cm->padh[2]];
			convertToYUVArrays(colBuf, image->Y, image->U, image->V);
			if (use_mv) {
				image->MVY = new int16_t[(cm->padw[0] * cm->padh[0] * 2) / (stride * stride)];
				image->MVUV = new int16_t[(cm->padw[1] * cm->padh[1] * 2) / (stride * stride)];
				convertToMVArrays(floatBuf, image->MVY, image->MVUV, stride);
			} else {
				image->MVY = NULL;
				image->MVUV = NULL;
			}
			c63_encode_image(cm, image);
		} else {
			color_vec.push_back(colBuf);
			mv_vec.push_back(floatBuf);
		}
		//movVecVals.write((char *)floatBuf, WINDOWHEIGHT*WINDOWWIDTH*sizeof(GLfloat)*2);


		checkError("Render3");

		// Render to the screen
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0,0,WINDOWWIDTH,WINDOWHEIGHT); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		// Clear the screen
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		checkError("RenderOmat");


		glUniformMatrix4fv(OldMatrixID, 1, GL_FALSE, &MVP[0][0]);
		checkError("RenderOmat2");

		// Use our shader
		glUseProgram(quad_programID);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, renderedTexture);
		// Set our "renderedTexture" sampler to user Texture Unit 0
		glUniform1i(texID, 0);

		glUniform1f(timeID, (float)(glfwGetTime()*10.0f) );

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
		glVertexAttribPointer(
				0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
				);

		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles

		glDisableVertexAttribArray(0);

		// Swap buffers
		glfwSwapBuffers();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS &&
			glfwGetWindowParam( GLFW_OPENED ) );
		int i, j, index;
		if (live_encoder) {
			fclose(cm->e_ctx.fp);
		}
		for (int v_el = 0; v_el < mv_vec.size(); v_el++) {
			writeMovVec(&movVecVals8, 8, v_el);
			writeMovVec(&movVecVals16, 16, v_el);
			int8_t tmp;
			int i, j, index;
			//for (i = WINDOWHEIGHT*WINDOWWIDTH*3; i <= 0; i-=3) {
			for (i = WINDOWHEIGHT-1; i >= 0; i--) {
				for (j = 0; j < WINDOWWIDTH; j++) {
					tmp = YVAL(color_vec[v_el], i*WINDOWWIDTH*3 + j*3);
					colorVals.write((char *)&tmp, 1);
				}
			}
			for (i = WINDOWHEIGHT-2; i >= 0; i-=2) {
				for (j = 0; j < WINDOWWIDTH; j+=2) {
					tmp = CBVAL(color_vec[v_el], i*WINDOWWIDTH*3 + j*3);
					colorVals.write((char *)&tmp, 1);
				}
			}
			for (i = WINDOWHEIGHT-2; i >= 0; i-=2) {
				for (j = 0; j < WINDOWWIDTH; j+=2) {
					tmp = CRVAL(color_vec[v_el], i*WINDOWWIDTH*3 + j*3);
					colorVals.write((char *)&tmp, 1);
				}
			}
		}
		colorVals.flush();
		for (int v_el = 0; v_el < mv_vec.size(); v_el++) {
			delete[] color_vec[v_el];
			delete[] mv_vec[v_el];
		}
		// Cleanup VBO and shader
		glDeleteBuffers(2, vertexbuffer);
		glDeleteBuffers(2, uvbuffer);
		glDeleteBuffers(2, normalbuffer);
		glDeleteBuffers(2, elementbuffer);
		glDeleteProgram(programID);
		glDeleteTextures(1, &TextureID);

		glDeleteFramebuffers(1, &FramebufferName);
		glDeleteTextures(1, &renderedTexture);
		//glDeleteRenderbuffers(1, &depthrenderbuffer);
		glDeleteRenderbuffers(3, depthTexture);
		glDeleteBuffers(1, &quad_vertexbuffer);
		glDeleteVertexArrays(1, &VertexArrayID);

		// Close OpenGL window and terminate GLFW
		glfwTerminate();
		colorVals.close();
		vertexVals.close();
		movVecVals8.close();
		movVecVals16.close();

		return 0;
	}

