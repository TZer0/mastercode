#version 330 core

layout(location = 0) in vec3 position;

layout(location = 0) out vec3 out_color;

layout(location = 0) uniform mat4 MVP;


void main(){
        // Apply the transformation uploaded from the CPU
        gl_Position =  MVP * vec4(position, 1);
        // We only vary the green-channel. Colour varies form corner to corner on the triangle displayed
        out_color = vec3(0.5, float(gl_VertexID)/2., 0.5);
}
