QT       += gui opengl

TARGET = SimpleGL
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    openglwindow.h

OTHER_FILES += \
    shader.fs \
    shader.vs
