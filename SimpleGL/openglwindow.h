#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H
#include <QtOpenGL/qgl.h>
#include <QMainWindow>
#include <QGLShaderProgram>
#include <QGLShader>
#include <QGLFunctions>

// QGLWidget is the base OpenGL-render widget in QT
// QGLFunctions grants additional functions such as glGenBuffer.
class OpenGLWindow : public QGLWidget, protected QGLFunctions
{
	Q_OBJECT

public:
	OpenGLWindow(QWidget *parent) : QGLWidget(parent)  {
		mFrag = mVert = NULL;
		mShaderProgram = NULL;
		mVertexBuffer = 0;
	}
	~OpenGLWindow() {
		if (mFrag) {
			delete mFrag;
		}
		if (mVert) {
			delete mVert;
		}
		if (mShaderProgram) {
			delete mShaderProgram;
		}
	}
	QGLShaderProgram *mShaderProgram;
	QGLShader *mVert, *mFrag;
	QMatrix4x4 mMVP;
	GLuint mVertexBuffer;
protected:

	void initializeGL()
	{
		// Required for QGLFunctions.
		initializeGLFunctions();

		// Section 1
		// Load and compiler the shader-files.
		mVert = new QGLShader(QGLShader::Vertex);
		mVert->compileSourceFile("../SimpleGL/shader.vs");
		mFrag = new QGLShader(QGLShader::Fragment);
		mFrag->compileSourceFile("../SimpleGL/shader.fs");
		// Add shader files to the shader program
		mShaderProgram = new QGLShaderProgram(this);
		mShaderProgram->addShader(mVert);
		mShaderProgram->addShader(mFrag);
		mShaderProgram->link();
		// Activate the shader program
		mShaderProgram->bind();

		// Section 2
		// Create some vertex positions
		GLfloat vertexPos[9];
		vertexPos[2] = vertexPos[5] = vertexPos[6] = vertexPos[8] = 0.0;
		vertexPos[0] = vertexPos[3] = vertexPos[4] = -1.0;
		vertexPos[1] = vertexPos[7] = 1.0;
		// These positions are equivalent to the ones which has been declared in Section 7.

		// Create buffer and insert buffer data
		glGenBuffers(1, &mVertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPos), vertexPos, GL_STATIC_DRAW);

		// Section 3
		// Reset matrix to identity
		mMVP.setToIdentity();
		// Set the field of view to 60, x:y-ratio to 1, near-plane to 0.2 and far-plane to 10
		mMVP.perspective(60, 1.0, 0.2, 10);
		// Move the camera to 2,2,2, looking at 0,0,0 with the up-direction being towards 0,1,0
		mMVP.lookAt(QVector3D(2,2,2), QVector3D(0,0,0), QVector3D(0,1,0));

		// Section 4
		// Enable Z-buffer
		glEnable(GL_DEPTH_TEST);
		// Set the clear colour to black.
		glClearColor(0.0, 0.0, 0.0, 1.0);
	}

	void resizeGL(int w, int h)
	{
		// In case of resize
		glViewport(0,0,w,h);
	}

	void paintGL()
	{
		// Section 5
		// Clear colour and Z-buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Upload the MVP-matrix in case of changes
		mShaderProgram->setUniformValue("MVP", mMVP);

		// Section 6
		// Bind and draw data
#if 1
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDisableVertexAttribArray(0);
#else
		// Section 7
		// Alternative (slower, will not work properly with shader):
		glBegin(GL_TRIANGLES);
		glVertex3f(-1, 1, 0.0);
		glVertex3f(-1, -1, 0.0);
		glVertex3f(0, 1, 0.0);
		glEnd();
#endif
	}
};

#endif
