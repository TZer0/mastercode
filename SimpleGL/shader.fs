#version 330 core

layout(location = 0) in vec3 in_color;

layout(location = 0) out vec4 color;

void main(){
        // Receive color, use it and set alpha channel to 100%
        color = vec4(in_color, 1.0);
}
