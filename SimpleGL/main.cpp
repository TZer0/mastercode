#include <QApplication>
#include "openglwindow.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	OpenGLWindow window(NULL);
	window.show();

	return a.exec();
}
